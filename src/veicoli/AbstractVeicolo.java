package veicoli;

public abstract class AbstractVeicolo{
    private String marca;
    private int anno;
    private int cavalli;

    public AbstractVeicolo(String marca, int anno, int cavalli) {
        this.marca = marca;
        this.anno = anno;
        this.cavalli = cavalli;
    }

    @Override
    public String toString() {
        return "AbstractVeicolo{" +
                "marca='" + marca + '\'' +
                ", anno=" + anno +
                ", cavalli=" + cavalli +
                '}';
    }
}
