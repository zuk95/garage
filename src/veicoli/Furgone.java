package veicoli;

public class Furgone extends AbstractVeicolo {
    private int capacita;

    public Furgone(String marca, int anno, int cavalli, int capacita) {
        super(marca, anno, cavalli);
        this.capacita = capacita;
    }

    @Override
    public String toString() {
        return super.toString()+"Furgone{" +
                "capacita=" + capacita +
                '}';
    }
}
