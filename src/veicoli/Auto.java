package veicoli;

public class Auto extends AbstractVeicolo {
    private int porte;
    private TipoAlimentazione alimentazione;

    public Auto(String marca, int anno, int cavalli, int porte, TipoAlimentazione alimentazione) {
        super(marca, anno, cavalli);
        this.porte = porte;
        this.alimentazione = alimentazione;
    }

    @Override
    public String toString() {
        return super.toString() +"Auto{" +
                "porte=" + porte +
                ", alimentazione=" + alimentazione +
                '}';
    }
}
