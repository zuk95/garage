package veicoli;

public class Moto extends  AbstractVeicolo {
    private int tempi;

    public Moto(String marca, int anno, int cavalli, int tempi) {
        super(marca, anno, cavalli);
        this.tempi = tempi;
    }

    @Override
    public String toString() {
        return super.toString()+"Moto{" +
                "tempi=" + tempi +
                '}';
    }
}
