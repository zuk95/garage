import veicoli.AbstractVeicolo;

import java.util.LinkedHashMap;
import java.util.Map;

public class Garage {
    private final int postiMax=20;
    private Map<String, AbstractVeicolo> posti;

    public Garage() {
        this.posti = new LinkedHashMap<>();
    }

public AbstractVeicolo addVeicolo(String numeroPosto,AbstractVeicolo veicolo) throws PostoNonEsistenteException{
        if(numeroPosto.contains("G")) {
            return posti.putIfAbsent(numeroPosto, veicolo);
        }else{
            throw new PostoNonEsistenteException("Posto non esistente");
        }
}

public AbstractVeicolo removeVeicolo(String numeroPosto){
        return posti.remove(numeroPosto);//eccezione se provo a rimuovere da un posto vuoto
}

    @Override
    public String toString() {
        return "Garage{" +
                "posti=" + posti+
                '}';
    }
}
