public class PostoNonEsistenteException extends Exception {
    public PostoNonEsistenteException(String message) {
        super(message);
    }
}
