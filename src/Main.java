import veicoli.*;

public class Main {

    public static void main(String[] args) {

        AbstractVeicolo ford = new Auto("FORD",2003,75,3, TipoAlimentazione.DIESEL);
        AbstractVeicolo fiat = new Furgone("FIAT",1999,90,500);
        AbstractVeicolo yamaha = new Moto("YAMAHA",2009,130,4);

        Garage primo = new Garage();

        System.out.println(primo);
try {
    primo.addVeicolo("G3", ford);
    primo.addVeicolo("G10", fiat);
    primo.addVeicolo("G18", yamaha);
    primo.addVeicolo("A30", ford);//posto non esistente,GESTIRE
}catch(PostoNonEsistenteException e){
    System.out.println(e.getMessage());
}
        System.out.println(primo);

        primo.removeVeicolo("G18");
        primo.removeVeicolo("G15");//posto non occupato

        System.out.println(primo);

    }
}
